grade = 5
while grade < 0 or grade > 1:
    try:
        grade = float(input("Unesite ocjenu:"))
    except: 
        print("Nije unesen broj.")

if grade >= 0.9:
 print("A") 
elif grade >= 0.8:
 print("B") 
elif grade >= 0.7:
  print("C") 
elif grade >= 0.6:
  print("D") 
elif grade < 0.6:
  print("F")